import sbt.*
import sbt.Keys.libraryDependencies

object Dependencies {
  object Versions {
    val zio            = "2.0.21"
    val tapir          = "1.9.6"
    val enumeratum     = "1.7.2"
    val quill          = "4.7.3"
    val testcontainers = "0.40.14"
  }

  object zio {
    val core    = "dev.zio" %% "zio"              % Versions.zio
    val test    = "dev.zio" %% "zio-test"         % Versions.zio % Test
    val testSbt = "dev.zio" %% "zio-test-sbt"     % Versions.zio
    val zioHttp = "dev.zio" %% "zio-http"         % "3.0.0-RC3"
    val zioJson = "dev.zio" %% "zio-json"         % "0.6.2"

    val all = List(core, test, testSbt, zioHttp)
  }

  object tapir {
    val core      = "com.softwaremill.sttp.tapir" %% "tapir-core"              % Versions.tapir
    val swaggerUi = "com.softwaremill.sttp.tapir" %% "tapir-swagger-ui-bundle" % Versions.tapir
    val zioJson   = "com.softwaremill.sttp.tapir" %% "tapir-json-zio"          % Versions.tapir
    val server    = "com.softwaremill.sttp.tapir" %% "tapir-zio-http-server"   % Versions.tapir

    val all = List(core, server, swaggerUi, zioJson)
  }

  object db {
    val quillCtx = "io.getquill"    %% "quill-zio"      % Versions.quill
    val quill    = "io.getquill"    %% "quill-jdbc-zio" % Versions.quill
    val postgres = "org.postgresql" %  "postgresql"     % "42.5.4"
    val h2       = "com.h2database" %  "h2"             % "2.1.214"

    val all = List(quill, postgres, h2, quillCtx)
  }

  object testContainers {
    val scala    = "com.dimafeng" %% "testcontainers-scala"            % Versions.testcontainers % Test
    val postgres = "com.dimafeng" %% "testcontainers-scala-postgresql" % Versions.testcontainers % Test

    val all = List(scala, postgres)
  }

  val enumeratum = "com.beachape" %% "enumeratum" % Versions.enumeratum
}
