CREATE TABLE IF NOT EXISTS task (
    id SERIAL PRIMARY KEY,
    description VARCHAR NOT NULL,
    notifyAt TIME,
    dayOfWeek VARCHAR
);