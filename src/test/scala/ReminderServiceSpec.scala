import com.dimafeng.testcontainers.PostgreSQLContainer
import io.getquill.Literal
import io.getquill.jdbczio.Quill
import zio.test.{Spec, TestEnvironment, ZIOSpecDefault, assertTrue}
import zio.{Scope, ZIO}
import zubrilin.telegram.reminder.db.DatabaseInterfacePostgresImpl
import zubrilin.telegram.reminder.model.Day.{Monday, Saturday, Sunday, Tuesday}
import zubrilin.telegram.reminder.model.Task
import zubrilin.telegram.reminder.service.{ReminderService, ReminderServiceImpl}

import java.time.LocalTime

object ReminderServiceSpec extends ZIOSpecDefault {
  val container = new PostgreSQLContainer()
  def acquire(initScript: String) = ZIO.attemptBlockingIO {
    val container = new PostgreSQLContainer().configure { c =>
      c.withExposedPorts(5432)
      c.withInitScript(initScript)
    }
    container.start()
    container
  }

  def release(container: => PostgreSQLContainer): ZIO[Any, Nothing, Unit] =
    ZIO.succeedBlocking(container.close())

  def source(initScript: String) =
    ZIO.acquireRelease(acquire(initScript))(release(_))

  def serviceFromContainer(container: => PostgreSQLContainer): ZIO[Any, Nothing, ReminderService] = ZIO.succeed {
    val pgDataSource = new org.postgresql.ds.PGSimpleDataSource()

    pgDataSource.setUser(container.container.getUsername)
    pgDataSource.setPassword(container.container.getPassword)
    pgDataSource.setURL(container.container.getJdbcUrl)

    val quill = new Quill.Postgres(Literal, pgDataSource)
    val databaseInterface = DatabaseInterfacePostgresImpl(quill)
    new ReminderServiceImpl(databaseInterface)
  }

  val cinemaTask = Task("Go to the cinema", LocalTime.MIDNIGHT, Saturday)
  val gymTask = Task("Go to the gym", LocalTime.parse("18:00"), Monday)
  val meetTask = Task("Meet with friends", LocalTime.parse("16:15"), Sunday)
  val ticketTask = Task("Buy ticket to the zoo", LocalTime.parse("10:00"), Tuesday)

  val mySchedule = List(cinemaTask, gymTask, meetTask)

  override def spec: Spec[TestEnvironment with Scope, Any] = suite("ReminderServiceSpec")(
    test("getSchedule") {
      for {
        container     <- source("schema.sql")
        reminder      <- serviceFromContainer(container)
        emptySchedule <- reminder.getSchedule
        _             <- reminder.createTask(cinemaTask)
        _             <- reminder.createTask(gymTask)
        _             <- reminder.createTask(meetTask)
        schedule      <- reminder.getSchedule
      } yield assertTrue(emptySchedule.isEmpty && schedule == mySchedule)
    },
    test("getTaskByID") {
      for {
        container      <- source("schema.sql")
        reminder       <- serviceFromContainer(container)
        id             <- reminder.createTask(cinemaTask)
        taskByCinemaId <- reminder.getTaskByID(id)
      } yield assertTrue(taskByCinemaId == cinemaTask)
    },
    test("createTask") {
      for {
        container      <- source("schema.sql")
        reminder       <- serviceFromContainer(container)
        id             <- reminder.createTask(ticketTask)
        taskByTicketId <- reminder.getTaskByID(id)
      } yield assertTrue(taskByTicketId == ticketTask)
    },
    test("deleteTask") {
      for {
        container      <- source("schema.sql")
        reminder       <- serviceFromContainer(container)
        id             <- reminder.createTask(ticketTask)
        _              <- reminder.deleteTask(id)
        taskByTicketId <- reminder.getTaskByID(id).either
      } yield assertTrue(taskByTicketId.isLeft)
    },
    test("service rebooted") {
      for {
        container     <- source("schema.sql")
        reminder      <- serviceFromContainer(container)
        emptySchedule <- reminder.getSchedule
        _             <- reminder.createTask(cinemaTask)
        _             <- reminder.createTask(gymTask)
        _             <- reminder.createTask(meetTask)
        schedule      <- reminder.getSchedule
        // service failed, reboot
        rebootedReminder <- serviceFromContainer(container)
        newSchedule      <- rebootedReminder.getSchedule
      } yield assertTrue(schedule == newSchedule)
    },
  )
}
