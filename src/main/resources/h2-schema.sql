CREATE TABLE IF NOT EXISTS task (
    id SERIAL PRIMARY KEY,
    description VARCHAR(200) NOT NULL,
    notifyAt TIME,
    dayOfWeek VARCHAR(10)
);