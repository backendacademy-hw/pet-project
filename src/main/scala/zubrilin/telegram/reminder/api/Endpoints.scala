package zubrilin.telegram.reminder.api

import sttp.tapir._
import sttp.tapir.json.zio._
import zubrilin.telegram.reminder.model.{ID, ReminderServiceError, Task}

object Endpoints {
  private val apiV1Endpoint = endpoint.in("api" / "v1").errorOut(plainBody[ReminderServiceError])
  private val reminder = apiV1Endpoint.in("reminder" / "schedule")
  private val reminderWithID = reminder.in(path[ID]("id"))

  val getSchedule: Endpoint[Unit, Unit, ReminderServiceError, List[Task], Any] = reminder.get
    .out(jsonBody[List[Task]])

  val getTask: Endpoint[Unit, ID, ReminderServiceError, Task, Any] = reminderWithID.get
    .out(jsonBody[Task])

  val createTask: Endpoint[Unit, Task, ReminderServiceError, ID, Any] = reminder.post
    .in("create-task")
    .in(
      jsonBody[Task]
        .description("Task to add")
        .example(Task.example),
    )
    .out(jsonBody[ID])

  val updateTask: Endpoint[Unit, (ID, Task), ReminderServiceError, ID, Any] = reminderWithID.put
    .in(
      jsonBody[Task]
        .description("Task to update")
        .example(Task.example),
    )
    .out(jsonBody[ID])

  val deleteTask: Endpoint[Unit, ID, ReminderServiceError, Unit, Any] = reminderWithID.delete

  val all = List(getSchedule, getTask, createTask, updateTask,  deleteTask)
}
