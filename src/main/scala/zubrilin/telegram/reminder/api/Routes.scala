package zubrilin.telegram.reminder.api

import sttp.tapir.server.ziohttp.ZioHttpInterpreter
import sttp.tapir.swagger.bundle.SwaggerInterpreter
import sttp.tapir.ztapir.*
import zio.http.HttpApp
import zubrilin.telegram.reminder.ReminderTask
import zubrilin.telegram.reminder.api.Endpoints.*
import zubrilin.telegram.reminder.service.ReminderService

object Routes {
  private val scheduleEndpoint: ZServerEndpoint[ReminderService, Any] =
    getSchedule.zServerLogic(_ => ReminderService.getSchedule)
  private val getTaskEndpoint: ZServerEndpoint[ReminderService, Any] =
    getTask.zServerLogic(id => ReminderService.getTaskByID(id))
  private val createTaskEndpoint: ZServerEndpoint[ReminderService, Any] =
    createTask.zServerLogic(task => ReminderService.createTask(task))
  private val updateTaskEndpoint: ZServerEndpoint[ReminderService, Any] =
    updateTask.zServerLogic { case (id, task) => ReminderService.updateTask(id, task) }
  private val deleteTaskEndpoint: ZServerEndpoint[ReminderService, Any] =
    deleteTask.zServerLogic(id => ReminderService.deleteTask(id))

  private val logicZEndpoints: List[ZServerEndpoint[ReminderService, Any]] = List(scheduleEndpoint, getTaskEndpoint, createTaskEndpoint, updateTaskEndpoint, deleteTaskEndpoint)
  private val swaggerZEndpoints: List[ZServerEndpoint[ReminderService, Any]] =
    SwaggerInterpreter().fromEndpoints[ReminderTask](Endpoints.all, "Reminder bot", "1.0")
  private val zEndpoints: List[ZServerEndpoint[ReminderService, Any]] = logicZEndpoints ++ swaggerZEndpoints

  val all: HttpApp[ReminderService] = ZioHttpInterpreter().toHttp(zEndpoints)
}
