package zubrilin.telegram.reminder.db

import zubrilin.telegram.reminder.model.{Day, ID, Task}

import java.time.LocalTime

case class TaskWithID(id: ID, description: String, notifyAt: LocalTime, dayOfWeek: Day) {
  def toTask: Task = Task(description, notifyAt, dayOfWeek)
}
object TaskWithID {
  def apply(id: ID, task: Task): TaskWithID = TaskWithID.apply(id, task.description, task.notifyAt, task.dayOfWeek)
  def apply(task: Task): TaskWithID = TaskWithID.apply(ID(0), task)
}
