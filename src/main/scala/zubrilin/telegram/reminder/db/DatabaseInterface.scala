package zubrilin.telegram.reminder.db

import io.getquill._
import io.getquill.jdbczio.Quill
import zio.{TaskLayer, URLayer, ZIO, ZLayer}
import zubrilin.telegram.reminder.model.{ID, Task}

import java.sql.SQLException
import javax.sql.DataSource

trait DatabaseInterface {
  def getSchedule: ZIO[Any, SQLException, List[Task]]
  def getTaskByID(id: ID): ZIO[Any, SQLException, Option[Task]]
  def createTask(task: Task): ZIO[Any, SQLException, ID]
  def updateTask(id: ID, task: Task): ZIO[Any, SQLException, ID]
  def deleteTask(id: ID): ZIO[Any, SQLException, Unit]
}

case class DatabaseInterfaceH2Impl(quill: Quill.H2[Literal]) extends DatabaseInterface {
  import quill._
  override def getSchedule: ZIO[Any, SQLException, List[Task]] =
    run(quote(querySchema[TaskWithID]("task"))).map(_.map(_.toTask))

  override def getTaskByID(id: ID): ZIO[Any, SQLException, Option[Task]] =
    run(quote(querySchema[TaskWithID]("task").filter(_.id == lift(id)))).map(_.map(_.toTask).headOption)

  override def createTask(task: Task): ZIO[Any, SQLException, ID] = {
    val taskWithID = TaskWithID(task)
    run(quote(querySchema[TaskWithID]("task").insertValue(lift(taskWithID)).returningGenerated(_.id))).map(id => ID(id.value))
  }

  override def updateTask(id: ID, task: Task): ZIO[Any, SQLException, ID] = {
    run(
      quote(
        querySchema[TaskWithID]("task")
          .filter(_.id == lift(id))
          .update(t => t.description -> lift(task.description), t => t.dayOfWeek -> lift(task.dayOfWeek), t => t.notifyAt -> lift(task.notifyAt)),
      ),
    ).map(id => ID(id.intValue))
  }

  override def deleteTask(id: ID): ZIO[Any, SQLException, Unit] =
    run(quote(querySchema[TaskWithID]("task").filter(_.id == lift(id)).delete)).unit
}

case class DatabaseInterfacePostgresImpl(val quill: Quill.Postgres[Literal]) extends DatabaseInterface {
  import quill._
  override def getSchedule: ZIO[Any, SQLException, List[Task]] =
    run(quote(querySchema[TaskWithID]("task"))).map(_.map(_.toTask))

  override def getTaskByID(id: ID): ZIO[Any, SQLException, Option[Task]] =
    run(quote(querySchema[TaskWithID]("task").filter(_.id == lift(id)))).map(_.map(_.toTask).headOption)

  override def createTask(task: Task): ZIO[Any, SQLException, ID] = {
    val taskWithID = TaskWithID(task)
    run(quote(querySchema[TaskWithID]("task").insertValue(lift(taskWithID)).returningGenerated(_.id))).map(id => ID(id.value))
  }

  override def updateTask(id: ID, task: Task): ZIO[Any, SQLException, ID] = {
    run(
      quote(
        querySchema[TaskWithID]("task")
          .filter(_.id == lift(id))
          .update(t => t.description -> lift(task.description), t => t.dayOfWeek -> lift(task.dayOfWeek), t => t.notifyAt -> lift(task.notifyAt)),
      ),
    ).map(id => ID(id.intValue))
  }

  override def deleteTask(id: ID): ZIO[Any, SQLException, Unit] =
    run(quote(querySchema[TaskWithID]("task").filter(_.id == lift(id)).delete)).unit
}

object DatabaseInterface {
  val dataSourceLive: TaskLayer[DataSource] = Quill.DataSource.fromPrefix("h2")
  val h2Live: URLayer[DataSource, Quill.H2[Literal]] = Quill.H2.fromNamingStrategy(Literal)
  val live: TaskLayer[DatabaseInterface] =
    dataSourceLive >>> h2Live >>> ZLayer.fromFunction(DatabaseInterfaceH2Impl.apply _)
}
