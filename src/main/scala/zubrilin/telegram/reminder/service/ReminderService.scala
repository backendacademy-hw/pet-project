package zubrilin.telegram.reminder.service

import zio.{ZIO, ZLayer}
import zubrilin.telegram.reminder.db.DatabaseInterface
import zubrilin.telegram.reminder.model.{ID, ReminderServiceError, Task}

trait ReminderService {
  def getSchedule: ZIO[Any, ReminderServiceError, List[Task]]
  def getTaskByID(id: ID): ZIO[Any, ReminderServiceError, Task]
  def createTask(task: Task): ZIO[Any, ReminderServiceError, ID]
  def updateTask(id: ID, task: Task): ZIO[Any, ReminderServiceError, ID]
  def deleteTask(id: ID): ZIO[Any, ReminderServiceError, Unit]
}

object ReminderService {
  def getSchedule: ZIO[ReminderService, ReminderServiceError, List[Task]] = ZIO.serviceWithZIO(_.getSchedule)
  def getTaskByID(id: ID): ZIO[ReminderService, ReminderServiceError, Task] = ZIO.serviceWithZIO(_.getTaskByID(id))
  def createTask(task: Task): ZIO[ReminderService, ReminderServiceError, ID] = ZIO.serviceWithZIO(_.createTask(task))
  def updateTask(id: ID, task: Task): ZIO[ReminderService, ReminderServiceError, ID] = ZIO.serviceWithZIO(_.updateTask(id, task))
  def deleteTask(id: ID): ZIO[ReminderService, ReminderServiceError, Unit] = ZIO.serviceWithZIO(_.deleteTask(id))

  val live: ZLayer[DatabaseInterface, Throwable, ReminderService] = ZLayer.fromFunction((db: DatabaseInterface) => new ReminderServiceImpl(db))

}

class ReminderServiceImpl(
    dataService: DatabaseInterface,
) extends ReminderService {
  override def getSchedule: ZIO[Any, ReminderServiceError, List[Task]] =
    (for {
      schedule <- dataService.getSchedule
    } yield schedule).mapError(th => ReminderServiceError.fromThrowable(th))

  override def getTaskByID(id: ID): ZIO[Any, ReminderServiceError, Task] = {
    for {
      taskOpt <- dataService.getTaskByID(id).mapError(th => ReminderServiceError.fromThrowable(th))
      task <- taskOpt match {
        case Some(t) => ZIO.succeed(t)
        case None    => ZIO.fail(ReminderServiceError.NoSuchID)
      }
    } yield task
  }

  override def createTask(task: Task): ZIO[Any, ReminderServiceError, ID] =
    for {
      id <- dataService.createTask(task).mapError(th => ReminderServiceError.fromThrowable(th))
    } yield id

  override def deleteTask(id: ID): ZIO[Any, ReminderServiceError, Unit] =
    for {
      _ <- dataService.deleteTask(id).mapError(th => ReminderServiceError.fromThrowable(th))
    } yield ()

  override def updateTask(id: ID, task: Task): ZIO[Any, ReminderServiceError, ID] =
    for {
      id <- dataService.updateTask(id, task).mapError(th => ReminderServiceError.fromThrowable(th))
    } yield id
}
