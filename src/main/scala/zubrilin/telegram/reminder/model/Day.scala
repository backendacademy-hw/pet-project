package zubrilin.telegram.reminder.model

import enumeratum._
import io.getquill.MappedEncoding
import sttp.tapir.Schema
import zio.json.{JsonDecoder, JsonEncoder}

sealed trait Day extends EnumEntry

object Day extends Enum[Day] {
  case object Monday extends Day
  case object Tuesday extends Day
  case object Wednesday extends Day
  case object Thursday extends Day
  case object Friday extends Day
  case object Saturday extends Day
  case object Sunday extends Day

  override val values: IndexedSeq[Day] = findValues

  given Schema[Day] = Schema.derived

  given JsonDecoder[Day] = JsonDecoder[String].map(Day.withName)
  given JsonEncoder[Day] = JsonEncoder[String].contramap(_.entryName)

  given MappedEncoding[Day, String] = MappedEncoding[Day, String](_.toString)
  given MappedEncoding[String, Day] = MappedEncoding[String, Day](Day.withName)
}
