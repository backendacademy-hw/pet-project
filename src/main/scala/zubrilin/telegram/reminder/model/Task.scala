package zubrilin.telegram.reminder.model

import sttp.tapir.Schema
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}
import zubrilin.telegram.reminder.model.Day.Wednesday

import java.time.LocalTime

case class Task(description: String, notifyAt: LocalTime, dayOfWeek: Day)

object Task extends TimeCodecs {
  val example = Task("Go to the gym", LocalTime.parse("19:00:00"), Wednesday)

  given JsonDecoder[Task] = DeriveJsonDecoder.gen[Task]
  given JsonEncoder[Task] = DeriveJsonEncoder.gen[Task]

  given Schema[Task] = Schema.derived
}
