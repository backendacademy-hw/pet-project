package zubrilin.telegram.reminder.model

import sttp.tapir.{Codec, DecodeResult}
import sttp.tapir.CodecFormat.TextPlain

sealed abstract class ReminderServiceError(val desc: String)

object ReminderServiceError {
  case object NoSuchID extends ReminderServiceError("No task with such id")
  case class Unknown(override val desc: String) extends ReminderServiceError(desc)
  def fromThrowable(th: Throwable) = Unknown(th.getMessage)

  implicit val codec: Codec[String, ReminderServiceError, TextPlain] = Codec.string.mapDecode {
    case NoSuchID.desc => DecodeResult.Value[ReminderServiceError](NoSuchID)
    case value         => DecodeResult.Error(value, new RuntimeException(s"Unable to decode value $value"))
  }(_.desc)
}
