package zubrilin.telegram.reminder.model

import zio.json.{JsonDecoder, JsonEncoder}

import java.time.LocalTime

trait TimeCodecs {
  given JsonDecoder[LocalTime] = JsonDecoder[String].map(LocalTime.parse)
  given JsonEncoder[LocalTime] = JsonEncoder[String].contramap(_.toString)
}
