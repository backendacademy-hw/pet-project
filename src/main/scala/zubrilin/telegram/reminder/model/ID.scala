package zubrilin.telegram.reminder.model

import io.getquill.MappedEncoding
import sttp.tapir.CodecFormat.TextPlain
import sttp.tapir.{Codec, DecodeResult, Schema}
import zio.json.{DeriveJsonDecoder, DeriveJsonEncoder, JsonDecoder, JsonEncoder}

case class ID(value: Int)

object ID {
  given JsonDecoder[ID] = DeriveJsonDecoder.gen[ID]
  given JsonEncoder[ID] = DeriveJsonEncoder.gen[ID]

  given Schema[ID] = Schema.derived

  given MappedEncoding[ID, Int] = MappedEncoding[ID, Int](_.value)
  given MappedEncoding[Int, ID] = MappedEncoding[Int, ID](ID.apply)

  private def decode(s: String): DecodeResult[ID] = DecodeResult.fromOption(s.toIntOption.map(ID.apply))

  private def encode(id: ID): String = id.toString

  given Codec[String, ID, TextPlain] = Codec.string.mapDecode(decode)(encode)
}
