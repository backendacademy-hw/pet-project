package zubrilin.telegram.reminder

import zio.http.*
import zio.Console.printLine
import zio.*
import zubrilin.telegram.reminder.api.Routes
import zubrilin.telegram.reminder.db.DatabaseInterface
import zubrilin.telegram.reminder.service.ReminderService

object App extends ZIOAppDefault {
  override val run =
    (for {
      fiber <- Server.serve(Routes.all.routes.toHttpApp).fork
      _     <- printLine(s"\nServer started on http://localhost:8080/")
      _     <- printLine(s"You can see docs on http://localhost:8080/docs")
      _     <- fiber.join
      _     <- ZIO.never
    } yield ()).provide(Server.default, ReminderService.live, DatabaseInterface.live, ZLayer.Debug.tree)
}
