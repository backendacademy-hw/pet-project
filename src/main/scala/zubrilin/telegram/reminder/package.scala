package zubrilin.telegram

import zio.ZIO
import zubrilin.telegram.reminder.service.ReminderService

package object reminder {
  type ReminderTask[A] = ZIO[ReminderService, Throwable, A]
}
