import Dependencies._

ThisBuild / scalaVersion := "3.3.1"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"
ThisBuild / organizationName := "example"

lazy val root = (project in file("."))
  .settings(
    name := "reminder",
    libraryDependencies ++=
      zio.all ++ tapir.all ++ db.all ++ testContainers.all :+ enumeratum,
  )
  .settings(
    dockerExposedPorts := Seq(8080),
    dockerBaseImage := "eclipse-temurin:19",
  )

enablePlugins(JavaAppPackaging, DockerPlugin)
